#export CXX="mipsel-openwrt-linux-musl-g++"
cmake_minimum_required(VERSION 3.22.1)
project(MQTT VERSION 0.1.0)

include(CTest)
enable_testing()

# # this one is important
 SET(CMAKE_SYSTEM_NAME Linux)
# #this one not so much
 SET(CMAKE_SYSTEM_VERSION 1)

# # specify the cross compiler
 SET(CMAKE_C_COMPILER   mipsel-openwrt-linux-musl-gcc)
 SET(CMAKE_CXX_COMPILER mipsel-openwrt-linux-musl-g++)

# # search for programs in the build host directories
 SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# # for libraries and headers in the target directories
 SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
 SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

add_executable(MQTT 
    src/mqtt_subled.cpp
)
target_include_directories(MQTT PUBLIC include)
target_link_libraries(MQTT mosquitto jsoncpp)

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)